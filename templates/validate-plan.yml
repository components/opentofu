spec:
  inputs:
    # Stages
    stage_validate:
      default: 'validate'
      description: 'Defines the validate stage. This stage includes the `fmt` and `validate` jobs.'
    stage_build:
      default: 'build'
      description: 'Defines the build stage. This stage includes the `plan` job.'

    # Versions
    # This version is only required, because we cannot access the context of the component,
    # see https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    version:
      default: 'latest'
      description: 'Version of this component. Has to be the same as the one in the component include entry.'

    base_os:
      default: 'alpine'
      options:
        - 'alpine'
        - 'debian'
        - '$GITLAB_OPENTOFU_BASE_IMAGE_OS'
      description: 'Base OS of GitLab OpenTofu image.'

    opentofu_version:
      default: '1.8.2'
      options:
        - '1.8.2'
        - '1.8.1'
        - '1.8.0'
        - '1.7.3'
        - '1.7.2'
        - '1.7.1'
        - '1.7.0'
        - '1.7.0-alpha1'
        - '1.6.2'
        - '1.6.1'
        - '1.6.0'
        - '$OPENTOFU_VERSION'
      description: 'OpenTofu version that should be used.'

    # Images
    image_registry_base:
      default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu'
      description: 'Host URI to the job images. Will be combined with `image_name` to construct the actual image URI.'
    # FIXME: not yet possible because of https://gitlab.com/gitlab-org/gitlab/-/issues/438722
    # gitlab_opentofu_image:
    #   # FIXME: This should reference the component tag that is used.
    #   #        Currently, blocked by https://gitlab.com/gitlab-org/gitlab/-/issues/438275
    #   # default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu/gitlab-opentofu:$[[ inputs.opentofu_version ]]'
    #   default: '$CI_TEMPLATE_REGISTRY_HOST/components/opentofu/gitlab-opentofu:$[[ inputs.version ]]-opentofu$[[ inputs.opentofu_version ]]'
    #   description: 'Tag of the gitlab-opentofu image.'

    image_name:
      default: 'gitlab-opentofu'
      description: 'Image name for the job images. Hosted under `image_registry_base`.'

    # Configuration
    root_dir:
      default: ${CI_PROJECT_DIR}
      description: 'Root directory for the OpenTofu project.'
    state_name:
      default: default
      description: 'Remote OpenTofu state name.'
    artifacts_access:
      default: 'none'
      description: 'Access level for the plan artifact. See https://docs.gitlab.com/ee/ci/yaml/#artifactsaccess for possible values.'

---

include:
  - local: '/templates/fmt.yml'
    inputs:
      as: 'fmt'
      stage: $[[ inputs.stage_validate ]]
      version: $[[ inputs.version ]]
      base_os: $[[ inputs.base_os ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
  - local: '/templates/validate.yml'
    inputs:
      as: 'validate'
      stage: $[[ inputs.stage_validate ]]
      version: $[[ inputs.version ]]
      base_os: $[[ inputs.base_os ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
  - local: '/templates/plan.yml'
    inputs:
      as: 'plan'
      stage: $[[ inputs.stage_build ]]
      version: $[[ inputs.version ]]
      base_os: $[[ inputs.base_os ]]
      opentofu_version: $[[ inputs.opentofu_version ]]
      image_registry_base: $[[ inputs.image_registry_base ]]
      image_name: $[[ inputs.image_name ]]
      root_dir: $[[ inputs.root_dir ]]
      state_name: $[[ inputs.state_name ]]
      artifacts_access: $[[ inputs.artifacts_access ]]
